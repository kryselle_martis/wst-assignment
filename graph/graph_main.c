#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "graph.h"

char **citynames;

int main(int argc, char *argv[]) {
	graph  *g;
	edge *T;
	int i, j;
	if(argc != 2)
		return EINVAL;
	g = createGraphFromFile(argv[1]);
	printPaths(*g);
	return 0;
}