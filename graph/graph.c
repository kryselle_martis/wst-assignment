#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "graph.h"



void initGraph(graph *g, int n){
    int i, j;
    g->a = (int **)malloc(sizeof(int *) * n);
    for(i = 0; i < n; i++) {
        g->a[i] = (int *)malloc(sizeof(int) * n);
        for(j = 0; j < n; j++)
            g->a[i][j] = 0;
    }
    g->n = n;
}

void insertEdge(graph *g, int i, int j, int weight) {
    if(i < 0 || i >= g->n || j < 0 || j >= g->n)
        return;
    g->a[i][j] = weight;
}

int readline(FILE *fp, char *line, int n) {
    int ch;
    int i = 0;
    while((ch = fgetc(fp)) != '\n' && ch != -1)
        line[i++] = ch;
    line[i] = '\0';
    return i;
}

int search(char *citynames[], char *name, int n) {
    int i;
    for(i = 0; i < n; i++)
        if(strcmp(citynames[i], name) == 0)
            return i;
    return -1;
}

graph *createGraphFromFile(char *filename) {
    FILE *fp;
    int n = 0, x, found, i, j;
    graph *g;
    char line[128], *token1, *token2, *token3;
    citynames = (char **)malloc(sizeof(char *) * 128);
    
    fp = fopen(filename, "r");
    if(fp == NULL)
        return NULL;
    g = (graph *)malloc(sizeof(graph));
    /* 1. Find n */
    while(1) {
        x = readline(fp, line, 128);
        if(x == 0)
            break;
        token1 = strtok(line, " \t");
        found = search(citynames, token1, n);
        if(found == -1) {
            citynames[n] = (char *)malloc(strlen(token1) + 1);
            strcpy(citynames[n], token1);
            n++;
        }
        token2 = strtok(NULL, " \t");
        found = search(citynames, token2, n);
        if(found == -1) {
            citynames[n] = (char *)malloc(strlen(token2) + 1);
            strcpy(citynames[n], token2);
            n++;
        }
    }
    /* 2. Init graph with n */
    initGraph(g, n);
    /* 3. fill in the adjancency matrix */
    fseek(fp, SEEK_SET, 0);
    while(1) {
        x = readline(fp, line, 128);
        if(x == 0)
            break;
        token1 = strtok(line, " \t");
        token2 = strtok(NULL, " \t");
        token3 = strtok(NULL, " \t\n");
        
        i = search(citynames, token1, n);	
        j = search(citynames, token2, n);	
        insertEdge(g, i, j, atoi(token3));
        insertEdge(g, j, i, atoi(token3));
    }		
    return g;
}

#define MAX 10000

result bellman_ford(graph g, int start) {
    result result;
    int *distance = (int *)malloc(sizeof(int) * g.n);
    int *parent = (int *)malloc(sizeof(int) * g.n);
    int i, j, x, count, new_distance;
    
    for(i = 0; i < g.n; i++) {
        if(i == start)
            distance[i] = 0;
        else
            distance[i] = MAX;
        parent[i] = -1;
    }
    
    for(x = 0; x < g.n - 1; x++) {
        count = 0;
        for(i = 0; i < g.n; i++) {
            for(j = 0; j < g.n; j++) {
                if(g.a[i][j]) {
                    new_distance = g.a[i][j] + distance[i];
                    if(new_distance < distance[j]) {
                        distance[j] = new_distance;
                        parent[j] = i;
                        count++;
                    }
                }
            }
        }
        if(count == 0)
            break;
    }
    result.parent = parent;
    result.distance = distance;
    return result;
}

void printpath(int *parent, int n, int start) {
    if(parent[start] == -1) {
        printf("%s ", citynames[start]);
        return;
    }
    printpath(parent, n, parent[start]);
    printf("%s ", citynames[start]);
}

void printPaths(graph g) {
    int i, j;
    result result;
    for( i = 0; i < g.n; i++) {
        result = bellman_ford(g, i);
        for(j = 0; j < g.n; j++) {
            printf("%4d: ", result.distance[j]);
            printpath(result.parent, g.n, j);
            printf("\n");
        } 
        printf("\n");
    }
}
